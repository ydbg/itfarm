package com.tc.itfarm.web.biz;

import com.tc.itfarm.model.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.struts.chain.contexts.ServletActionContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

@Service
public class LoginBiz {

	@Resource
	private HttpServletRequest request;

	/**
	 * 获取当前登陆用户
	 * @author: wangdongdong
	 * @date: 2016年7月13日       
	 * @return
	 */
	public User getCurUser() {
		return (User) request.getSession().getAttribute("user");
	}
}
