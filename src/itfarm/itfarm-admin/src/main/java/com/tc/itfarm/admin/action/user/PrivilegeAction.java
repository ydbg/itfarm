package com.tc.itfarm.admin.action.user;

import com.tc.itfarm.api.common.JsonMessage;
import com.tc.itfarm.model.Privilege;
import com.tc.itfarm.service.PrivilegeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by wangdongdong on 2016/8/16.
 */
@Controller
@RequestMapping("/privilege")
public class PrivilegeAction {

    @Resource
    private PrivilegeService privilegeService;

    @RequestMapping("list")
    public String list() {
        return "admin/privilege/privilege";
    }

    /**
     * 权限管理列表
     * @return
     */
    @RequestMapping(value = "/treeGrid", method = RequestMethod.POST)
    @ResponseBody
    public Object dataGrid() {
        return privilegeService.selectAll();
    }

    @RequestMapping("addUI")
    public String addUI() {
        return "admin/privilege/privilegeAdd";
    }

    @RequestMapping("editUI")
    public String editUI(ModelMap model, @RequestParam(value = "id", required = true) Integer id) {
        Privilege privilege = privilegeService.select(id);
        model.addAttribute("privilege", privilege);
        return "admin/privilege/privilegeEdit";
    }

    @RequestMapping("save")
    @ResponseBody
    public JsonMessage save(Privilege privilege) {
        Integer result = privilegeService.save(privilege);
        return JsonMessage.toResult(result, JsonMessage.STATUS_SUCCESS_MSG, "保存失败");
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public JsonMessage delete(@RequestParam(value = "id", required = true) Integer id) {
        Integer result = privilegeService.delete(id);
        return JsonMessage.toResult(result, JsonMessage.STATUS_SUCCESS_MSG, "删除失败");
    }

    @RequestMapping(value = "/allTree", method = RequestMethod.POST)
    @ResponseBody
    public Object allTree() {
        return privilegeService.selectAllTree();
    }


    @RequestMapping(value = "/privilegeTree", method = RequestMethod.POST)
    @ResponseBody
    public Object privilegeTree() {
        return privilegeService.selectPrivilegetree();
    }
}
