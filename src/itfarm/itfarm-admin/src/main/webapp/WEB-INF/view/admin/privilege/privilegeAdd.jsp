<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<script type="text/javascript">
    $(function() {

        $('#pid').combotree({
            url : '${path }/privilege/allTree.do',
        });

        $('#resourceAddForm').form({
            url : '${path }/privilege/save.do',
            onSubmit : function() {
                progressLoad();
                var isValid = $(this).form('validate');
                if (!isValid) {
                    progressClose();
                }
                return isValid;
            },
            success : function(result) {
                progressClose();
                result = $.parseJSON(result);
                if (result.success) {
                    parent.$.modalDialog.openner_treeGrid.treegrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_treeGrid这个对象，是因为resource.jsp页面预定义好了
                    //parent.layout_west_tree.tree('reload');
                    parent.$.modalDialog.handler.dialog('close');
                }
            }
        });

        $('#icon_select').change(function () {
            alert('sad');
            alert(this.value);
            $("#icon_img").attr("data-options", "plain:true,iconCls:'"+this.value+"'");
        })
    });
</script>
<div style="padding: 3px;">
    <form id="resourceAddForm" method="post">
        <table class="grid">
            <tr>
                <td>资源名称</td>
                <td><input name="privilegeName" type="text" placeholder="请输入资源名称" class="easyui-validatebox span2" data-options="required:true" ></td>
                <td>资源类型</td>
                <td><select name="resourcetype" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                            <option value="0">菜单</option>
                            <option value="1">按钮</option>
                </select></td>
            </tr>
            <tr>
                <td>权限码</td>
                <td><input name="privilegeCode" type="text" placeholder="请输入权限码" class="easyui-validatebox span2" data-options="required:true" ></td>
                <td>资源路径</td>
                <td><input name="url" type="text" placeholder="请输入资源路径" class="easyui-validatebox span2" data-options="width:140,height:29" ></td>
            </tr>
            <tr>
                <td>菜单图标</td>
                <td ><select name="iconCls" id="icon_select" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                    <option value="icon-company">company</option>
                    <option value="icon-folder">folder</option>
                    <option value="icon-home">home</option>
                    <option value="icon-btn">btn</option>
                </select><a class="easyui-linkbutton" id="icon_img" data-options="plain:true,iconCls:'icon-company'"></a></td>
                <td>状态</td>
                <td ><select name="status" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                            <option value="0">正常</option>
                            <option value="1">停用</option>
                </select></td>
            </tr>
            <tr>
                <td>上级资源</td>
                <td colspan="3"><select id="pid" name="pid" style="width: 200px; height: 29px;"></select>
                <a class="easyui-linkbutton" href="javascript:void(0)" onclick="$('#pid').combotree('clear');" >清空</a></td>
            </tr>
        </table>
    </form>
</div>